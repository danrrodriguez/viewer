import React from 'react';
import './App.css';
//import DocViewer, {PDFRenderer, PNGRenderer, TIFFRenderer, JPGRenderer, MSDocRenderer} from "react-doc-viewer";
import DocViewer, { DocViewerRenderers } from "react-doc-viewer";
//import TifComponent from "./Componente.tsx"



function App() {
  const docs = [
    { uri: require("./docs/Captura de pantalla (1).png") },
    { uri: require("./docs/índice.jpg") },
    { uri: require("./docs/índice2.jpeg") },
    { uri: require("./docs/Doc1.pdf") },
    { uri: require("./docs/img1.tiff") },
    { uri: require("./docs/img2.tiff") }, // Local File
    { uri: require("./docs/img3.tif") },
    { uri: require("./docs/img4.tiff") },
    { uri: require("./docs/jmeter_tutorial.pdf") },
    { uri: require("./docs/Copia de CUADRO_REQ_UGPP - ULTIMO.xlsx")}, // Local File
  ];

  return <DocViewer 
    //pluginRenderers={[TifComponent, TIFFRenderer, PDFRenderer, PNGRenderer, JPGRenderer, MSDocRenderer ]}
    pluginRenderers={DocViewerRenderers}
    documents={docs} />
    
}

export default App;
