import React from "react";
import { DocRenderer } from "react-doc-viewer";
 
const TifComponent: DocRenderer = ({
  mainState: { currentDocument },
}) => {
  if (!currentDocument) return null;
 
  return (
    <div id="my-tiff-renderer">
      <img id="tiff-img" src={currentDocument.fileData as string} />
    </div>
  );
}

 
TifComponent.fileTypes = ["tiff", "image/tiff"];
TifComponent.weight = 1;

export default TifComponent;